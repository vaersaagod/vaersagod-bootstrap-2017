#!/bin/bash

function coloredEcho {
    local exp=$1;
    local color=$2;
    local mod=$3;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $mod "$exp";
    tput sgr0;
}

function installPlugin {
  local url=$1;
  local directory=$2;
  local path="vendor/$2";

  coloredEcho "Downloading $url" white
  git clone --depth=1 --quiet $url $path
  rm -rf "$path/.git"

  if [ -d "$path/$directory" ]; then
    mv $path vendor/tmp
    mv "vendor/tmp/$directory" $path
    rm -rf vendor/tmp
  fi
}

function chuckNorris {
  curl -s http://api.icndb.com/jokes/random/ | python -c 'import sys, json; print "\n"+json.load(sys.stdin)["value"]["joke"]+"\n"'
}

function program_is_installed {
  local val=1
  type $1 >/dev/null 2>&1 || { local val=0; }
  echo "$val"
}

####### DETECT NEEDED SOFTWARE #######
SOFTWARE_OK=1

if [[ -f ~/.nvm/nvm.sh ]]; then
  . ~/.nvm/nvm.sh
else
  coloredEcho "✗ This script needs nvm installed on your system." red
  coloredEcho "See https://github.com/creationix/nvm#installation" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed node) == 1 ]]; then
  NODE=$(which node)
else
  coloredEcho "✗ This script needs node installed on your system." red
  coloredEcho "See https://nodejs.org/en/download/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed mysql) == 1 ]]; then
  MYSQL=$(which mysql)
else
  coloredEcho "✗ This script needs mysql installed on your system." red
  coloredEcho "See https://dev.mysql.com/downloads/mysql/" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $(program_is_installed yarn) == 1 ]]; then
  YARN=$(which yarn)
else
  coloredEcho "✗ The new boilerplate needs yarn installed on your system." red
  coloredEcho "See https://yarnpkg.com/en/docs/install" red
  echo ''
  SOFTWARE_OK=0
fi

if [[ $SOFTWARE_OK == 0 ]]; then
  exit
fi

####### CHUCK NORRIS #######
#chuckNorris

####### USER INPUT #######
coloredEcho "Project directory (or ENTER for current): " cyan -n
read projectDir

####### HANDLE DIRECTORY INPUT #######
if [[ -z $projectDir ]]; then
  coloredEcho "Using current directory..." green
else
  projectDirSafe=${projectDir//[^a-zA-Z0-9.]/}
  if [[ -d "$projectDirSafe" ]]; then
    coloredEcho "✗ The directory '$projectDirSafe' already exits. Overwrite it? [y/N]: " red -n
    read projectDirOverwrite
    if [[ $projectDirOverwrite =~ ^[Yy]$ ]]; then
      rm -rf $projectDirSafe
    else
      coloredEcho "✗ Okay then, you figure it out..." red
      exit
    fi
  fi
  coloredEcho "Creating directory '$projectDirSafe' switching to it..." green
  mkdir -- "$projectDirSafe" && cd -- "$projectDirSafe"
  echo ''
fi

coloredEcho "Project handle (will be used for local database AND *.test prefix): " cyan -n
read projectname

coloredEcho "Project description (will be used for package.json): " cyan -n
read projectDesc

coloredEcho "Space separated list of locales (or ENTER for none): " cyan -n
read projectlocales

coloredEcho "Download default plugins? [y/N]: " cyan -n
read downloadPlugins

coloredEcho "Download and use the latest LTS of Node? [y/N]: " cyan -n
read updateNode

####### CREATE DATABASE #######
echo ''
export MYSQL_PWD=root
coloredEcho "Creating database craftcms_$projectname..." green
$MYSQL -u root -e "CREATE DATABASE IF NOT EXISTS craftcms_$projectname";
coloredEcho "✔ DONE" white

####### NVM/NODE SETUP #######
if [[ $updateNode =~ ^[Yy]$ ]]; then
  echo ''
  lts=$(nvm version-remote --lts)
  coloredEcho "Downloading and installing the latest LTS of Node: $lts..." green
  echo $lts > .nvmrc
  nvm install
else
  echo $(nvm current) > .nvmrc
fi

####### DOWNLOAD CRAFT #######
echo ''
coloredEcho 'Downloading the latest version of Craft...' green

mkdir -p tmp
curl -L http://buildwithcraft.com/latest.zip?accept_license=yes -o tmp/Craft.zip
unzip -q tmp/Craft.zip
rm -rf tmp

rm -rf public
rm -rf craft/templates
rm -rf craft/config
rm -rf craft/plugins
rm -rf craft/storage
find . -name 'web.config' -type f -delete
find . -name 'readme.txt' -type f -delete

coloredEcho "✔ DONE" white

####### DOWNLOAD CRAFT PLUGINS #######
echo ''
if [[ $downloadPlugins =~ ^[Yy]$ ]]; then
  coloredEcho 'Downloading default plugins...' green

  installPlugin https://github.com/aelvan/Inlin-Craft.git inlin
  installPlugin https://github.com/aelvan/Stamp-Craft.git stamp
  installPlugin https://github.com/aelvan/Imager-Craft.git imager
  installPlugin https://github.com/aelvan/Preparse-Field-Craft.git preparsefield
  installPlugin https://github.com/aelvan/FocalPointField-Craft.git focalpointfield
  installPlugin https://github.com/fruitstudios/LinkIt.git fruitlinkit
  installPlugin https://github.com/engram-design/ImageResizer.git imageresizer
  installPlugin https://github.com/nystudio107/seomatic.git seomatic
  installPlugin https://github.com/nystudio107/minify.git minify
  installPlugin https://github.com/nystudio107/cookies.git cookies
  installPlugin https://github.com/mmikkel/CpFieldLinks-Craft.git cpfieldlinks
  installPlugin https://github.com/mmikkel/RetconHTML-Craft.git retconhtml
  installPlugin https://github.com/verbb/field-manager.git fieldmanager
else
  coloredEcho 'Downloading required plugins...' green

  installPlugin https://github.com/aelvan/Inlin-Craft.git inlin
  installPlugin https://github.com/aelvan/Stamp-Craft.git stamp
fi

coloredEcho "✔ DONE" white

###### VÆRSÅGOD FANNYPACK ######
echo ''
coloredEcho 'Downloading the Fannypack...' green

mkdir -p tmp
git clone --depth=1 --quiet git@bitbucket.org:vaersaagod/vaersagod-fannypack.git tmp
rm -rf tmp/.git

cp -r ./tmp/vrsg ./vendor/vrsg
rm -rf tmp

coloredEcho "✔ DONE" white

####### VÆRSÅGOD BOILERPLATE #######
echo ''
coloredEcho 'Downloading Vrsg Boilerplate...' green

mkdir -p tmp
git clone --depth=1 --quiet git@bitbucket.org:vaersaagod/vaersagod-boilerplate-2017.git tmp
rm -rf tmp/.git

find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_NAME/${projectname}/g" {} \;
find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" -o -name ".env-example" \) -exec sed -i '' "s/VRSG_PROJECT_DESC/${projectDesc}/g" {} \;
cp -r ./tmp/. ./
rm -rf tmp

####### SETTING UP LOCALES IF ANY #######
for locale in ${projectlocales//,/ }
do
  coloredEcho "Setting up locale $locale..." green
  mkdir public/$locale
  cp -R public/locale/ public/$locale
  find ./public/$locale -type f \( -name "*.php" -o -name "*.htaccess" \) -exec sed -i '' "s/VRSG_PROJECT_LOCALE/${locale}/g" {} \;
done
rm -rf public/locale

$NODE ./scripts/locales.js "${projectlocales}"
rm scripts/locales.js

####### CLEANING UP #######
mv .env-example .env
rm -f .env-example
rm -f yarn.lock

coloredEcho "✔ DONE" white

####### SETTING PERMISSIONS #######
echo ''
coloredEcho 'Setting permission levels...' green
permLevel=774
chmod $permLevel craft/app
chmod $permLevel config
chmod $permLevel storage
coloredEcho "✔ DONE" white

####### CREATE VALET HOST IF AVAILABLE #######
if [[ $(program_is_installed valet) == 1 ]]; then
  VALET=$(which valet)
  coloredEcho "Setting up and securing host https://$projectname.test through valet..." green
  $VALET link $projectname && $VALET secure $projectname;
  coloredEcho "✔ DONE" white

  echo ''
    coloredEcho 'Now...' green
    coloredEcho " 1) Check settings in the .env file" magenta
    coloredEcho " 3) Run the installer at https://$projectname.test/admin" magenta
    coloredEcho " 4) Install needed plugins https://$projectname.test/admin/plugins" magenta
    coloredEcho " 5) Type yarn && yarn start" magenta
else
  echo ''
    coloredEcho 'Now...' green
    coloredEcho " 1) Create a host for $projectname.test" magenta
    coloredEcho " 2) Check settings in the .env file" magenta
    coloredEcho " 3) Run the installer at http://$projectname.test/admin" magenta
    coloredEcho " 4) Install needed plugins http://$projectname.test/admin/plugins" magenta
    coloredEcho " 5) Type yarn && yarn start" magenta
fi

echo ''
coloredEcho 'Make something awesome!' white
